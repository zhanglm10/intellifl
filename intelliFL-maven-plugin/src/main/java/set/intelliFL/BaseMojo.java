/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.Iterator;
import java.util.List;

import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Plugin;
import org.apache.maven.plugin.BuildPluginManager;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugin.surefire.SurefirePlugin;
import org.apache.maven.plugin.surefire.util.DirectoryScanner;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.apache.maven.surefire.booter.Classpath;
import org.apache.maven.surefire.booter.SurefireExecutionException;
import org.apache.maven.surefire.testset.TestListResolver;
import org.apache.maven.surefire.util.DefaultScanResult;

import set.intelliFL.log.IntelliFLLogger;
import set.intelliFL.techs.PRFL;

public class BaseMojo extends SurefirePlugin
{
	/**
	 * Location of the build target.
	 */
	@Parameter(defaultValue = "${project.build.directory}", property = "classesDir", required = true)
	private File outputDirectory;

	/**
	 * The current maven project under test
	 */
	@Parameter(property = "project")
	protected MavenProject mavenProject;

	/**
	 * The dir of the maven project under test
	 */
	@Parameter(defaultValue = "${basedir}")
	protected File baseDir;

	@Parameter(property = PRFL.SPEC_TECH, defaultValue = "PR_Ochiai")
	protected String specTech;

	@Parameter(property = PRFL.AGG, defaultValue = "false")
	protected boolean aggregation;

	@Parameter(property = "skipIT", defaultValue = "false")
	protected boolean skipIT;

	/**
	 * Version of the current faulttracer
	 */
	@Parameter(defaultValue = "1.0-SNAPSHOT")
	protected String version;

	/**
	 * The level of coverage to collect
	 */
	@Parameter(defaultValue = "meth-cov")
	protected String coverageLevel;

	/**
	 * The prefix for the packages under analysis. If none provided, coverage of
	 * all packages will be traced
	 */
	@Parameter(property = "prefix")
	protected String prefix;

	/**
	 * The location of the agent jar
	 */
	@Parameter(property = "agentJar", defaultValue = "")
	protected String agentJar;

	@Component
	protected MavenSession mavenSession;
	@Component
	protected BuildPluginManager pluginManager;

	protected Plugin surefire;
	protected Plugin failsafe;
	protected Classpath sureFireClassPath;

	protected Plugin lookupPlugin(String paramString) {
		List<Plugin> localList = this.mavenProject.getBuildPlugins();
		Iterator<Plugin> localIterator = localList.iterator();
		while (localIterator.hasNext()) {
			Plugin localPlugin = localIterator.next();
			if (paramString.equalsIgnoreCase(localPlugin.getKey())) {
				return localPlugin;
			}
		}
		return null;
	}

	protected String getPathToIntelliFLJar() {
		/*
		 * getIntelliFLVersion(); String localRepo =
		 * this.mavenSession.getSettings().getLocalRepository(); String result =
		 * Paths .get(mavenSession.getLocalRepository().getBasedir(), "set",
		 * "intelliFL", "intelliFL-core", version, "intelliFL-core-" + version +
		 * ".jar") .toString();
		 */

		File jarFile = new File(Properties.class.getProtectionDomain()
				.getCodeSource().getLocation().getPath());
		String result = jarFile.toString();
		return result;
	}

	protected void getIntelliFLVersion() {
		Artifact artifact = (Artifact) this.mavenProject.getPluginArtifactMap()
				.get("set.intelliFL:intelliFL-maven-plugin");
		this.version = artifact.getVersion();
	}

	protected String getArguments() {

		String arguments = Properties.AGENT_JAR_KEY + this.agentJar + ","
				+ Properties.COV_TYPE_KEY + this.coverageLevel + ","
				+ Properties.BASE_DIR_KEY + this.baseDir.toString() + ","
				+ Properties.CLS_DIR_KEY
				+ getClassesDirectory().getAbsolutePath() + ","
				+ Properties.TIMING_KEY + Properties.TIMING;

		Properties.serializeConfig(getClassesDirectory().getAbsolutePath() , this.baseDir.toString());
		IntelliFLLogger.debug(" Mojo arguments: "+arguments);

		return arguments;
	}



	/*
	 * (non-Javadoc)
	 * 
	 * @see org.apache.maven.plugin.Mojo#execute()
	 */
	@Override
	public void execute() throws MojoExecutionException, MojoFailureException {
		// TODO Auto-generated method stub

	}

	public Classpath getSurefireClasspath() throws MojoExecutionException {

		if (sureFireClassPath == null) {
			try {
				List<String> paths = getProject().getTestClasspathElements();
				// List<String> paths =
				// getProject().getCompileClasspathElements();
				// List<String> paths =
				// getProject().getSystemClasspathElements();
				sureFireClassPath = new Classpath(paths);
			} catch (DependencyResolutionRequiredException drre) {
				drre.printStackTrace();
			}
		}
		return sureFireClassPath;
	}

	public ClassLoader createClassLoader(Classpath sfClassPath) {
		ClassLoader loader = null;
		try {
			loader = sfClassPath.createClassLoader(null, false, false,
					"MyLoader");
		} catch (SurefireExecutionException see) {
			see.printStackTrace();
		}
		return loader;
	}

	public static String getSlashName(String clazz) {
		return clazz.replace(".", "/") + ".class";
	}

	protected List<String> getAllSrcClasses() {
		// System.out.println("+++"+getClassesDirectory().getAbsolutePath());
		DirectoryScanner classScanner = new DirectoryScanner(
				getClassesDirectory(), new TestListResolver("*"));
		DefaultScanResult scanResult = classScanner.scan();
		return scanResult.getFiles();
	}
}
