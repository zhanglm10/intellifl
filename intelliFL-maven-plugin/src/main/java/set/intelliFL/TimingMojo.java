package set.intelliFL;

import java.io.File;

import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Execute;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;

/**
 * This mojo can record the start time and end time of each test case
 * 
 * 
 * @author Administrator
 *
 */
@Mojo(name = "timing", requiresDependencyResolution = ResolutionScope.TEST)
@Execute(phase = LifecyclePhase.TEST_COMPILE)
public class TimingMojo extends BaseCovMojo{
	public void execute() throws MojoExecutionException  {
		Properties.TIMING=true;
		//checkFile();
		super.execute();
	}
	public void checkFile(){
		File timingDir = new File(Properties.TIMING_PATH);
		if (timingDir.exists()){
			for(File file: timingDir.listFiles()) 
				if (!file.isDirectory()) 
					file.delete();
		}else{
			try{
				timingDir.mkdirs();
			} 
			catch(SecurityException se){} 
		}
	}

}
