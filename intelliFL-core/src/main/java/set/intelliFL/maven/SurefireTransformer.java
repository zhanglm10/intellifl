/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.maven;

import java.lang.instrument.ClassFileTransformer;
import java.lang.instrument.IllegalClassFormatException;
import java.security.ProtectionDomain;

import org.objectweb.asm.ClassReader;
import org.objectweb.asm.ClassWriter;
import org.objectweb.asm.Opcodes;


public class SurefireTransformer implements ClassFileTransformer, Opcodes
{

	@Override
	public byte[] transform(ClassLoader loader, String className,
			Class<?> classBeingRedefined, ProtectionDomain protectionDomain,
			byte[] classfileBuffer) throws IllegalClassFormatException {
		if (className == null) {
			return classfileBuffer;
		}
		/*if (loader != ClassLoader.getSystemClassLoader()) {
			return classfileBuffer;
		}*/
		if ((className
				.equals("org/apache/maven/plugin/surefire/AbstractSurefireMojo"))
				|| (className
						.equals("org/apache/maven/plugin/surefire/SurefirePlugin"))
				|| (className.equals(
						"org/apache/maven/plugin/failsafe/IntegrationTestMojo"))) {
			ClassReader localClassReader = new ClassReader(classfileBuffer);
			ClassWriter localClassWriter = new ClassWriter(localClassReader,
					ClassWriter.COMPUTE_FRAMES);
			SurefireClassVisitor localMavenClassVisitor = new SurefireClassVisitor(
					localClassWriter);
			localClassReader.accept(localMavenClassVisitor,
					ClassReader.EXPAND_FRAMES);
			return localClassWriter.toByteArray();
		}
		return null;
	}

}
