/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.maven;

import java.lang.reflect.Field;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import set.intelliFL.Properties;
import set.intelliFL.log.IntelliFLLogger;

public class SurefireRewriter
{
	public static final String className = "set/intelliFL/maven/SurefireRewriter";
	public static ConcurrentMap map = new ConcurrentHashMap();

	public static void execute(Object plugin) throws Exception {
		String className = plugin.getClass().getName();
		// check if indeed the class is related to mvn test
		if (!className.equals("org.apache.maven.plugin.surefire.SurefirePlugin")
				&& !className.equals(
						"org.apache.maven.plugin.failsafe.IntegrationTestMojo")) {
			return;
		}
		// check if the plugin is already transformed
		if (map.put(plugin, 1) != null) {
			return;
		}
		// check and update the argLine parameter
		checkUpdateArgLine(plugin);

		// ignore test failure and proceed
		if(className.equals("org.apache.maven.plugin.surefire.SurefirePlugin"))
			setAttribute("testFailureIgnore", plugin, true);
	}

	private static void checkUpdateArgLine(Object plugin) throws Exception {
		String argLine = (String) getAttribute("argLine", plugin);

		String agentLine = "-javaagent:" + Properties.AGENT_JAR + "="
				+ Properties.AGENT_ARG /*
										 * + "," + Properties.BASE_DIR_KEY +
										 * System.getProperty(Properties.
										 * BASE_DIR_KEY) + "," +
										 * Properties.CLS_DIR_KEY +
										 * System.getProperty(Properties.
										 * CLS_DIR_KEY)
										 */;
		String newArgLine = agentLine;
		if (argLine != null)
			newArgLine = agentLine + " " + argLine;
		IntelliFLLogger.info(newArgLine);
		setAttribute("argLine", plugin, newArgLine);
	}

	protected static void setAttribute(String attr, Object plugin,
			List<String> list) throws Exception {
		Field localField = null;
		try {
			localField = plugin.getClass().getDeclaredField(attr);
		} catch (NoSuchFieldException localNoSuchFieldException) {
			localField = plugin.getClass().getSuperclass()
					.getDeclaredField(attr);
		}
		localField.setAccessible(true);
		localField.set(plugin, list);
	}

	protected static Object getAttribute(String name, Object plugin)
			throws Exception {
		Field localField = null;
		try {
			localField = plugin.getClass().getDeclaredField(name);
		} catch (NoSuchFieldException localNoSuchFieldException) {
			localField = plugin.getClass().getSuperclass()
					.getDeclaredField(name);
		}
		localField.setAccessible(true);
		return localField.get(plugin);
	}

	protected static void setAttribute(String fieldName, Object plugin,
			Object value) throws Exception {
		Field field;
		try {
			field = plugin.getClass().getDeclaredField(fieldName);
		} catch (NoSuchFieldException ex) {
			field = plugin.getClass().getSuperclass()
					.getDeclaredField(fieldName);
		}
		field.setAccessible(true);
		field.set(plugin, value);
	}

}
