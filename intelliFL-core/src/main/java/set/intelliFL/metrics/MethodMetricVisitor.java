/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.metrics;

import org.objectweb.asm.Handle;
import org.objectweb.asm.Label;
import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

class MethodMetricVisitor extends MethodVisitor implements Opcodes
{
	MetricData data;
	String name;

	public MethodMetricVisitor(final MethodVisitor mv, String name,
			MetricData data) {
		super(ASM5, mv);
		this.data = data;
		this.name=name;
	}

	@Override
	public void visitFrame(int type, int nLocal, Object[] local, int nStack,
			Object[] stack){
		data.numFrame++;
	}
	@Override
	public void visitInsn(int opcode) {
		data.numInsn++;
	}

	@Override
	public void visitIntInsn(int opcode, int operand) {
		data.numIntInsn++;
	}

	@Override
	public void visitVarInsn(int opcode, int var) {
		data.numVarInsn++;
	}

	@Override
	public void visitTypeInsn(int opcode, String desc) {
		data.numTypeInsn++;
	}

	@Override
	public void visitFieldInsn(int opc, String owner, String name,
			String desc) {
		data.numFieldInsn++;
	}

	@Override
	public void visitMethodInsn(int opc, String owner, String name,
			String desc) {
		data.numMethInsn++;
	}

	@Override
	public void visitInvokeDynamicInsn(String name, String desc, Handle bsm,
			Object... bsmArgs) {
		data.numInvokeDynamicInsn++;
	}

	@Override
	public void visitJumpInsn(int opcode, Label label) {
		data.numJumpInsn++;
	}

	@Override
	public void visitLabel(Label label) {
		data.numLabelInsn++;
	}

	@Override
	public void visitLdcInsn(Object cst) {
		data.numLabelInsn++;
	}

	@Override
	public void visitIincInsn(int var, int increment) {
		data.numIincInsn++;
	}

	@Override
	public void visitTableSwitchInsn(int min, int max, Label dflt,
			Label[] labels) {
		data.numTableSwitchInsn++;
	}

	@Override
	public void visitLookupSwitchInsn(Label dflt, int[] keys, Label[] labels) {
		data.numLookupSwitchInsn++;
	}

	@Override
	public void visitMultiANewArrayInsn(String desc, int dims) {
		data.numMultiANewArrayInsn++;
	}

	@Override
	public void visitTryCatchBlock(Label start, Label end, Label handler,
			String type) {
		data.numTryCatchBlock++;
	}

}