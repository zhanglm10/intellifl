/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.cg;

import java.io.IOException;
import java.util.jar.JarFile;

public class FindReflectionReachableMethods
{
	public static void main(String[] args) throws IOException {
		String[] spots = {
				"java/lang/Class:forName(Ljava/lang/String;)Ljava/lang/Class;",
				"java/lang/Class:forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;",
				"java/lang/ClassLoader:loadClass(Ljava/lang/String;)Ljava/lang/Class;",
				"java/lang/ClassLoader:loadClass(Ljava/lang/String;Z)Ljava/lang/Class;",
				"java/lang/ClassLoader:findSystemClass(Ljava/lang/String;)Ljava/lang/Class;" };

		long time1 = System.currentTimeMillis();
		JarFile jar = new JarFile(
				"/Library/Java/JavaVirtualMachines/jdk1.8.0_101.jdk/Contents/Home/jre/lib/rt.jar");
		CallGraphBuilder.analyzeInheritance(jar);
		CallGraphBuilder.analyzeCG(jar);
		long time2 = System.currentTimeMillis();
		System.out.println(
				"CG construction cost: " + (time2 - time1) / 1000 + "s");
		 CallGraphBuilder.findReachableClasses(spots);
		 CallGraphBuilder.findReachableMethods(spots);

		long time3 = System.currentTimeMillis();
		System.out.println("CG traverse cost: " + (time3 - time2) / 1000 + "s");

		CallGraph.printPath("string-init-path",
				Data.getEncoding(
						"java/lang/Class:forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;"),
				Data.getEncoding("java/lang/String:<init>([B)V"));
		CallGraph.printPath("string-split-path",
				Data.getEncoding(
						"java/lang/Class:forName(Ljava/lang/String;ZLjava/lang/ClassLoader;)Ljava/lang/Class;"),
				Data.getEncoding(
						"java/lang/String:split(Ljava/lang/String;)[Ljava/lang/String;"));
	}


}
