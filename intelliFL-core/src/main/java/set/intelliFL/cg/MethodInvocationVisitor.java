/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.cg;

import java.util.Set;

import org.objectweb.asm.MethodVisitor;
import org.objectweb.asm.Opcodes;

class MethodInvocationVisitor extends MethodVisitor implements Opcodes
{

	String callerId;

	public MethodInvocationVisitor(final MethodVisitor mv, String name) {
		super(ASM5, mv);
		this.callerId = name;
	}

	@Override
	public void visitMethodInsn(int opcode, String owner, String name,
			String desc, boolean itf) {
		if (!Data.methodNameMap.containsKey(name + desc)
				|| !Data.classNameMap.containsKey(owner))
			return;
		// System.out.println(owner);
		int calleeMethodId = Data.methodNameMap.get(name + desc);
		int calleeClassId = Data.classNameMap.get(owner);
		String calleeId = calleeClassId + ":" + calleeMethodId;
		// do not need lookup in case of special or static calls
		if (opcode == Opcodes.INVOKESPECIAL) {
			CallGraph.reverseCG.addVertex(callerId);
			CallGraph.reverseCG.addVertex(calleeId);
			CallGraph.reverseCG.addEdge(calleeId, callerId);
			// System.out.println(callerId + "==>" + owner + ":" + name + desc);
		} else {
			CallGraph.reverseCG.addVertex(callerId);
			String actualCalleeId = Data.getFirstSuperclassWithMethod(
					calleeClassId, calleeMethodId);
			if(actualCalleeId==null){
				return;
			}
			CallGraph.reverseCG.addVertex(callerId);
			CallGraph.reverseCG.addVertex(actualCalleeId);
			CallGraph.reverseCG.addEdge(actualCalleeId, callerId);

			if (opcode == Opcodes.INVOKEVIRTUAL
					|| opcode == Opcodes.INVOKEINTERFACE) {
				Set<String> spring = Data.getTransitiveSubclasses(owner);
				for (String clazz : spring) {
					calleeClassId = Data.classNameMap.get(clazz);
					if (Data.classMethodMap.get(calleeClassId)
							.contains(calleeMethodId)) {
						calleeId = calleeClassId + ":" + calleeMethodId;
						CallGraph.reverseCG.addVertex(callerId);
						CallGraph.reverseCG.addVertex(calleeId);
						CallGraph.reverseCG.addEdge(calleeId, callerId);
						// System.out.println(callerId + "==>"+
						// Data.classNameMap.inverse().get(calleeClassId)+ ":" +
						// name + desc);
					}
				}
			}
		}

		// mv.visitMethodInsn(opcode, owner, name, desc, itf);
	}

}