/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.instrument;

import java.util.BitSet;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;


public class CoverageData
{
	public static boolean WITH_RTINFO = false;
	public final static String TRACER = "set/intelliFL/instrument/Tracer";
	public static int classId = 0;
	public static ConcurrentMap<String, Integer> classIdMap = new ConcurrentHashMap<String, Integer>();
	public static ConcurrentMap<Integer, String> idClassMap = new ConcurrentHashMap<Integer, String>();
	// public static ConcurrentMap<String, String> classNameMap = new
	// ConcurrentHashMap<String, String>();
	public static ConcurrentMap<Integer, ConcurrentMap<Integer, String>> idMethMap = new ConcurrentHashMap<Integer, ConcurrentMap<Integer, String>>();
	public final static int MAX_CLASSNUM = 100000;

	// store method coverage: array is approximatedly 0.5 second faster than
	// bitset for joda-time
	public static boolean[][] methCovArray = new boolean[MAX_CLASSNUM][];
	// store statement coverage
	public static BitSet[][] stmtCovSet = new BitSet[MAX_CLASSNUM][];
	public final static String CLINIT = "<clinit>:()V";

	public static int registerClass(String slashClazz, String dotClazz) {
		int id = nextId();
		classIdMap.put(dotClazz, id);
		idClassMap.put(id, slashClazz);
		return id;
	}

	public static int registerMeth(int clazzId, String meth) {
		if (!idMethMap.containsKey(clazzId)) {
			// Map map = new ConcurrentHashMap<Integer, String>();
			ConcurrentMap<Integer, String> map = new ConcurrentHashMap<Integer, String>();
			map.put(0, CLINIT);
			idMethMap.put(clazzId, map);
		}
		if (meth.equals(CLINIT))
			return 0;
		int id = idMethMap.get(clazzId).size();
		idMethMap.get(clazzId).put(id, meth);
		return id;
	}

	private synchronized static int nextId() {
		return classId++;
	}

	public static int decodeClassId(final long value) {
		return (int) (value >> 32);
	}

	public static int decodeMethodId(final long value) {
		return (int) (value & 0xFFFFFFFF);
	}

}
