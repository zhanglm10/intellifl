package set.intelliFL.instrument;

import org.objectweb.asm.Opcodes;

public class MethodFilter
{
	/**
	 * Check if the method is synthetic, if so, skip instrumentation
	 * @param access
	 * @return
	 */
	public static boolean shouldInstrument(int access){
		return (access & Opcodes.ACC_BRIDGE)==0;
	}

}
