/**
 * The MIT License
 * Copyright © 2017 The intelliFL team
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package set.intelliFL.junit;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Set;

import org.junit.runner.Description;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

import set.intelliFL.Properties;
import set.intelliFL.io.TracerIO;

public class FTracerJUnitRunner
{
	static final String className = "set/intelliFL/junit/FTracerJUnitRunner";
	static final String dumpCov = "dumpCoverage";
	static final String recordFailure = "recordFailure";
	static final String recordStart = "recordStart";
	static final String recordFinish = "recordFinish";
	static final String dumpTiming = "dumpTiming";
	static Set<String> failedTests = new HashSet<String>();
	static HashMap<String, Long[]> testsTiming = new HashMap<String, Long[]>();

	public static void dumpCoverage(Description d) throws IOException {
		String test = d.getClassName() + "." + d.getMethodName();

		dumpCoverage(test);
	}

	public static void dumpTiming(Result r) throws IOException {
		String timingFile = Properties.BASE_DIR + File.separator
				+ Properties.IFL_DIR + File.separator + Properties.TIMING_DIR
				+ File.separator + Properties.TIMING_FILE;
		File file = new File(timingFile);
		file.getParentFile().mkdirs();
		BufferedWriter writer = new BufferedWriter(
				new FileWriter(timingFile, true));
		for (String t : testsTiming.keySet()) {
			writer.write(t + " " + testsTiming.get(t)[0] + " "
					+ testsTiming.get(t)[1] + "\n");
		}
		writer.flush();
		writer.close();
	}

	public static void recordFailure(Failure f) throws IOException {
		Description d = f.getDescription();
		String test = d.getClassName() + "." + d.getMethodName();
		failedTests.add(test);
	}

	public static void recordStart(Description d) throws IOException {
		String test = d.getClassName() + "." + d.getMethodName();
		Long startTime = System.nanoTime();
		Long[] Timing = { startTime, 0L };
		testsTiming.put(test, Timing);
	}

	public static void recordFinish(Description d) throws IOException {
		String test = d.getClassName() + "." + d.getMethodName();
		Long finishTime = System.nanoTime();
		testsTiming.get(test)[1] = finishTime;
	}

	private static void dumpCoverage(String test) throws IOException {
		String fileName = Properties.BASE_DIR + File.separator
				+ Properties.IFL_DIR + File.separator + Properties.IFL_COV_DIR
				+ File.separator + getFileName(test) + Properties.GZ_EXT;

		File file = new File(fileName);
		file.getParentFile().mkdirs();
		boolean outcome = !failedTests.contains(test);
		if (Properties.METH_COV.equals(Properties.COV_TYPE))
			TracerIO.writeMethodCov(test, outcome, fileName);
		else if (Properties.STMT_COV.equals(Properties.COV_TYPE))
			TracerIO.writeStmtCov(test, outcome, fileName);
	}

	public static String getFileName(String test) {
		return org.apache.commons.codec.digest.DigestUtils.sha1Hex(test);
	}

	public static boolean isParameterizedTest(String testClass) {
		return testClass.startsWith("[");
	}

}
